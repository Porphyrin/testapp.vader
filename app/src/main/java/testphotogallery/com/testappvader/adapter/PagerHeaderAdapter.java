package testphotogallery.com.testappvader.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.View;

import testphotogallery.com.testappvader.R;
import testphotogallery.com.testappvader.fragment.HeaderUserInfoFragment;
import testphotogallery.com.testappvader.model.UserModel;


public class PagerHeaderAdapter extends FragmentPagerAdapter {
    private final FragmentManager fm;
    private UserModel userModel;


    private HeaderUserInfoFragment fragment;

    public PagerHeaderAdapter(FragmentManager fm, UserModel userModel) {
        super(fm);
        this.fm = fm;

        this.userModel = userModel;
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        fragment = HeaderUserInfoFragment.newInstance(userModel, position);
        return fragment;

    }


}