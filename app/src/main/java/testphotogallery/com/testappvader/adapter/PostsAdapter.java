package testphotogallery.com.testappvader.adapter;


import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import testphotogallery.com.testappvader.R;
import testphotogallery.com.testappvader.databinding.ItemGridviewBinding;
import testphotogallery.com.testappvader.model.PostModel;


public class PostsAdapter extends BaseAdapter {
    private ArrayList<PostModel> postsList;


    public PostsAdapter(ArrayList<PostModel> photosList) {
        this.postsList = photosList;
    }

    @Override
    public int getCount() {

        return postsList.size();
    }

    @Override
    public PostModel getItem(int position) {

        return postsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gridview, parent, false);
            ItemGridviewBinding binding = DataBindingUtil.bind(convertView);
            holder = new ViewHolder();
            holder.binding = binding;
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        holder.binding.setPost(getItem(position));


        return convertView;
    }


    private static class ViewHolder {
        ItemGridviewBinding binding;
    }

}
