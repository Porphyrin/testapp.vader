package testphotogallery.com.testappvader;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;



public class GApplication extends Application {


    public static Context context;
    public static RequestQueue requestQueue;


    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(this);

    }


    private static RequestQueue getRequestQueue() {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(GApplication.context);
        }
        return requestQueue;
    }

    public static <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
