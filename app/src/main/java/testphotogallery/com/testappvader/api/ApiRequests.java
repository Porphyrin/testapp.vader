package testphotogallery.com.testappvader.api;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import testphotogallery.com.testappvader.helper.SharedPreferences;


public class ApiRequests<T> extends Request<T> {


    public static final String API_ENDPOINT = "https://api.parse.com/1/";
    public static final String API_ENDPOINT_LOGIN = API_ENDPOINT.concat("login");
    public static final String API_ENDPOINT_USERINFO = API_ENDPOINT.concat("functions/get_user");

    private Response.ErrorListener errorListener;
    public Class<T> clazz;
    private String url = "";
    public Response.Listener<T> listener;
    private Map<String, Object> params;


    public ApiRequests(

            String url,
            Response.Listener<T> reponseListener,
            Response.ErrorListener errorListener,
            Class<T> clazz

    ) {
        super(0, url, errorListener);

        this.listener = reponseListener;
        this.url = url;
        this.errorListener = errorListener;

        this.clazz = clazz;


    }


    public ApiRequests(
            int method,
            Map<String, Object> params,
            String url,
            Response.Listener<T> reponseListener,
            Response.ErrorListener errorListener,
            Class<T> clazz

    ) {
        super(method, url, errorListener);

        this.listener = reponseListener;
        this.url = url;
        this.params = params;
        this.errorListener = errorListener;
        this.clazz = clazz;


    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Parse-Application-Id", "yYcDIIHX6E1csMs3vYDX7gtzO5b2VTIxqvG4ir5C");
        headers.put("X-Parse-REST-API-Key", "2lXf7PBYERNdwrIeCqTiisdqYjFJJjLg6j8aMStN");

        if (!SharedPreferences.getString(SharedPreferences.PARSE_SESSION_TOKEN).isEmpty())
            headers.put("X-Parse-Session-Token", SharedPreferences.getString(SharedPreferences.PARSE_SESSION_TOKEN));

        return headers;
    }


    protected Map getParams() throws com.android.volley.AuthFailureError {
        return params;
    }


    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String jsonString = new String(response.data);


        return Response.success(new Gson().fromJson(jsonString, clazz), null);
    }


    @Override
    protected void deliverResponse(T response) {

        if (listener != null)
            listener.onResponse(response);
    }


}