package testphotogallery.com.testappvader.model;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import testphotogallery.com.testappvader.helper.Utils;

public class UserModel implements Serializable {


    private String name;
    private String nicName;
    private int following;
    private int followers;
    private String avatarThumbUrl;
    private String avatarUrl;


    @BindingAdapter({"bind:imgUrl"})
    public static void loadImage(final ImageView view, final String url) {
        if (url == null) return;

        Picasso.with(view.getContext())
                .load(url)
                .into(view);

    }





    @BindingAdapter({"bind:imgBlurredUrl"})
    public static void loadBlurredImage(final ImageView view, final String url) {
        if (url == null) return;

        Picasso.with(view.getContext())
                .load(url)
                .into(view, new Callback() {
                    @Override
                    public void onSuccess() {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Bitmap blurredBt = Utils.blur(((BitmapDrawable) view.getDrawable()).getBitmap());

                                if (blurredBt != null)
                                    view.setImageBitmap(blurredBt);
                            }
                        }, 100);

                    }

                    @Override
                    public void onError() {
                        Log.e("PICASSO", "onError ");
                    }
                });

    }


    public void setName(String name) {
        this.name = name;
    }

    public void setNicName(String nicName) {
        this.nicName = nicName;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public void setAvatarThumbUrl(String avatarThumbUrl) {
        this.avatarThumbUrl = avatarThumbUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getAvatarThumbUrl() {
        return avatarThumbUrl;
    }

    public String getName() {
        return name;
    }

    public String getNicName() {
        return nicName;
    }

    public String getFollowing() {
        return String.valueOf(following).concat(" Following");
    }

    public String getFollowers() {
        return String.valueOf(followers).concat(" Followers");
    }


}
