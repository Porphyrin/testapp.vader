package testphotogallery.com.testappvader.model;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class PostModel {

    public String bgUrl;
    public String avatarUrl;
    private int commentsCount;
    private int likesCount;

    @BindingAdapter({"bind:imgUrl"})
    public static void loadImage(final ImageView view, final String url) {
        if (url == null) return;
        Picasso.with(view.getContext())
                .load(url)
                .into(view);
    }





    public void setBackgroundUrl(String thumbUrl) {
        this.bgUrl = thumbUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    public String getBackgroundUrl() {
        return bgUrl;
    }


    public String getAvatarUrl() {
        return avatarUrl;
    }


    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public String getLikesCount() {
        return String.valueOf(likesCount);
    }
}
