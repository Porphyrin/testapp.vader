package testphotogallery.com.testappvader.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import testphotogallery.com.testappvader.R;
import testphotogallery.com.testappvader.databinding.FragmentHeaderUserInfoBinding;
import testphotogallery.com.testappvader.model.UserModel;


public class HeaderUserInfoFragment extends Fragment {

    private int position;
    private UserModel model;
    private FragmentHeaderUserInfoBinding binding;


    public HeaderUserInfoFragment() {
    }

    public static HeaderUserInfoFragment newInstance(UserModel model, int position) {

        HeaderUserInfoFragment frag = new HeaderUserInfoFragment();


        Bundle extras = new Bundle();
        extras.putSerializable("model", model);
        extras.putInt("position", position);

        frag.setArguments(extras);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = (UserModel) getArguments().getSerializable("model");
        position = getArguments().getInt("position");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_header_user_info, container, false);
        binding.setUser(model);

        if (position == 0)
            binding.page1.setVisibility(View.VISIBLE);
        else
            binding.page1.setVisibility(View.GONE);


        return binding.getRoot();
    }


}