package testphotogallery.com.testappvader.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import testphotogallery.com.testappvader.GApplication;
import testphotogallery.com.testappvader.R;
import testphotogallery.com.testappvader.adapter.PagerHeaderAdapter;
import testphotogallery.com.testappvader.adapter.PostsAdapter;
import testphotogallery.com.testappvader.api.ApiRequests;
import testphotogallery.com.testappvader.databinding.ActivityProfileBinding;
import testphotogallery.com.testappvader.helper.SharedPreferences;
import testphotogallery.com.testappvader.model.PostModel;
import testphotogallery.com.testappvader.model.UserModel;

public class ProfileActivity extends AppCompatActivity {

    private ActivityProfileBinding binding;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);


        Intent intent = getIntent();

        if (intent == null) return;

        userModel = (UserModel) intent.getSerializableExtra("userModel");
        PagerHeaderAdapter adapter = new PagerHeaderAdapter(getSupportFragmentManager(), userModel);
        binding.viewPager.setAdapter(adapter);
        binding.setUser(userModel);


        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {

            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (positionOffset == 0f) return;

                binding.page2.setScaleX(positionOffset);
                binding.page2.setScaleY(positionOffset);

            }

            public void onPageSelected(int position) {

                if (position == 0) {
                    binding.paginatorPage1.setTextColor(Color.WHITE);
                    binding.paginatorPage2.setTextColor(getResources().getColor(R.color.paginator_desabled));
                } else {
                    binding.paginatorPage1.setTextColor(getResources().getColor(R.color.paginator_desabled));
                    binding.paginatorPage2.setTextColor(Color.WHITE);
                }


            }
        });


        getUserInfo();


    }

    private void getUserInfo() {


        ApiRequests<JsonObject> request = new ApiRequests<>(Request.Method.POST, null, ApiRequests.API_ENDPOINT_USERINFO, new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                if (response != null) {
                    parseUserInfoResponse(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(ProfileActivity.this, R.string.error_login, Toast.LENGTH_LONG).show();
                SharedPreferences.putString(SharedPreferences.PARSE_SESSION_TOKEN, "");
                finish();
            }
        }, JsonObject.class);

        GApplication.addToRequestQueue(request);
    }

    private void parseUserInfoResponse(JsonObject response) {

        JsonObject userObj = response.getAsJsonObject("result").getAsJsonObject("user");
        if (userObj.has("posts")) {
            JsonArray postsArray = userObj.get("posts").getAsJsonArray();

            ArrayList<PostModel> postsList = new ArrayList<>();

            for (int i = 0, max = postsArray.size(); i < max; i++) {
                JsonObject postObj = (JsonObject) postsArray.get(i);
                PostModel postModel = new PostModel();
                if (postObj.has("by_avatar"))
                    postModel.setAvatarUrl(postObj.get("by_avatar").getAsString());
                if (postObj.has("image"))
                    postModel.setBackgroundUrl(postObj.get("image").getAsString());
                if (postObj.has("comments_count"))
                    postModel.setCommentsCount(postObj.get("comments_count").getAsInt());
                if (postObj.has("likes_count"))
                    postModel.setLikesCount(postObj.get("likes_count").getAsInt());
                postsList.add(postModel);
            }

            binding.gridView.setExpanded(true);
            binding.gridView.setAdapter(new PostsAdapter(postsList));


        }


    }


}
