package testphotogallery.com.testappvader.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;

import java.util.HashMap;

import testphotogallery.com.testappvader.GApplication;
import testphotogallery.com.testappvader.R;
import testphotogallery.com.testappvader.api.ApiRequests;
import testphotogallery.com.testappvader.helper.SharedPreferences;
import testphotogallery.com.testappvader.model.UserModel;


public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        callAuth();
    }

    private void callAuth() {

        ApiRequests<JsonObject> request = new ApiRequests<>(ApiRequests.API_ENDPOINT_LOGIN.concat("?username=bibi@gmail.com&password=123456"), new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {

                if (response != null && response.has("sessionToken")) {
                    parseAuthResponse(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(SplashActivity.this, R.string.error_login, Toast.LENGTH_LONG).show();
                SharedPreferences.putString(SharedPreferences.PARSE_SESSION_TOKEN, "");
                finish();
            }
        }, JsonObject.class);

        GApplication.addToRequestQueue(request);


    }

    private void parseAuthResponse(JsonObject response) {


        if (response.has(SharedPreferences.PARSE_SESSION_TOKEN)) {
            SharedPreferences.putString(SharedPreferences.PARSE_SESSION_TOKEN, response.get(SharedPreferences.PARSE_SESSION_TOKEN).getAsString());

            UserModel userModel = new UserModel();
            if (response.has("display_name") && !response.get("display_name").isJsonNull())
                userModel.setName(response.get("display_name").getAsString());
            if (response.has("nickname") && !response.get("nickname").isJsonNull())
                userModel.setNicName(response.get("nickname").getAsString());
            if (response.has("numOfFollowers") && !response.get("numOfFollowers").isJsonNull())
                userModel.setFollowers(response.get("numOfFollowers").getAsInt());
            if (response.has("avatar_thumbnail") && !response.get("avatar_thumbnail").isJsonNull())
                userModel.setAvatarThumbUrl(response.get("avatar_thumbnail").getAsString());
            if (response.has("avatar") && !response.get("avatar").isJsonNull())
                userModel.setAvatarUrl(response.get("avatar").getAsString());

            Intent intent = new Intent(SplashActivity.this, ProfileActivity.class);
            intent.putExtra("userModel", userModel);
            startActivity(intent);
            finish();
        }

    }
}
