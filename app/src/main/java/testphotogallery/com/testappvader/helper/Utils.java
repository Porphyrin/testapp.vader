package testphotogallery.com.testappvader.helper;

import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Log;

import testphotogallery.com.testappvader.GApplication;

/**
 * Created by admin on 3/18/16.
 */
public class Utils {
    private Utils() {
    }


    public static Bitmap blur(Bitmap input) {
        if (null == input) return null;


        try {
            RenderScript rsScript = RenderScript.create(GApplication.context);
            Allocation alloc = Allocation.createFromBitmap(rsScript, input);

            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript));
            blur.setRadius(10f);
            blur.setInput(alloc);

            Bitmap result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888);
            Allocation outAlloc = Allocation.createFromBitmap(rsScript, result);

            blur.forEach(outAlloc);
            outAlloc.copyTo(result);

            rsScript.destroy();
            return result;
        } catch (Exception e) {
            Log.e("BLUR", e.getMessage());
            return null;
        }

    }


}
