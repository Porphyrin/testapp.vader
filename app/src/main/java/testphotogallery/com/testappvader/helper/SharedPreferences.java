package testphotogallery.com.testappvader.helper;


import android.content.Context;
import android.location.Location;

import com.google.gson.Gson;

import testphotogallery.com.testappvader.GApplication;

public class SharedPreferences {

    private SharedPreferences() {
    }


    public static final String PARSE_SESSION_TOKEN = "sessionToken";


    public static void putLocation(String key, Location value) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor prefsEditor = myPrefs.edit();
        String location = new Gson().toJson(value);
        prefsEditor.putString(key, location);
        prefsEditor.commit();
    }

    public static void putBoolean(String key, boolean value) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public static Boolean getBoolean(String key) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        return myPrefs.getBoolean(key, false);

    }

    public static void putInteger(String key, Integer value) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public static Integer getInteger(String key) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        return myPrefs.getInt(key, 0);

    }

    public static Double getLong(String key) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        return Double.longBitsToDouble(myPrefs.getLong(key, 0));

    }

    public static void putString(String key, String value) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static String getString(String key) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        return myPrefs.getString(key, "");

    }

    public static void putLong(String key, Long value) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putLong(key, value);
        prefsEditor.commit();
    }


    public static Location getLocation(String key) {
        android.content.SharedPreferences myPrefs = GApplication.context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        return new Gson().fromJson(myPrefs.getString(key, ""), Location.class);
    }

}
